package com.enigmacamp.crud_room.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.enigmacamp.crud_room.room.artist.Artist
import com.enigmacamp.crud_room.room.artist.ArtistDao
import com.enigmacamp.crud_room.room.song.Song
import com.enigmacamp.crud_room.room.song.SongDao

@Database(entities = [Artist::class, Song::class], version = 2, exportSchema = false)
abstract class ArtistRoomDatabase: RoomDatabase(){

    companion object {
        @Volatile
        private var INSTANCE : ArtistRoomDatabase? = null

        fun getDatabase(context: Context): ArtistRoomDatabase {
            return INSTANCE ?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ArtistRoomDatabase::class.java,
                    "db_sepotifay"
                )
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                instance
            }
        }
    }

    abstract fun getArtistDao(): ArtistDao
    abstract fun getArtishWithSongDao(): ArtistWithSongDao
    abstract fun getSongDao(): SongDao

}