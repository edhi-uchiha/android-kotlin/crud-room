package com.enigmacamp.crud_room.room

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import com.enigmacamp.crud_room.room.artist.Artist
import com.enigmacamp.crud_room.room.song.Song
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ArtistWithSong(
    @Embedded
    val artist: Artist,

    @Relation(
        parentColumn = "id",
        entityColumn = "authorId"
    )
    val songs: List<Song>
) : Parcelable