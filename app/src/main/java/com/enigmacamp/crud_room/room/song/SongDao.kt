package com.enigmacamp.crud_room.room.song

import androidx.room.*
import com.enigmacamp.crud_room.room.artist.Artist

@Dao
interface SongDao {

    @Insert
    fun create(song: Song)

    @Update
    fun update(song: Song)

    @Delete
    fun delete(song: Song)

    @Query("SELECT * FROM song")
    fun getAll(): List<Song>

    @Query("SELECT * FROM song WHERE id = :id")
    fun getById(id: Int): List<Song>
}