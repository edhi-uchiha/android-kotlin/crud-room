package com.enigmacamp.crud_room.room.artist

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enigmacamp.crud_room.R
import com.enigmacamp.crud_room.room.ArtistWithSong
import com.enigmacamp.crud_room.room.song.Song

class ArtistAdaper (
    private val listItem: ArrayList<ArtistWithSong>,
    private val listener: ArtistListener
): RecyclerView.Adapter<ArtistAdaper.ArtistViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistViewHolder {
       val view = LayoutInflater.from(parent.context).inflate(R.layout.list_artist, parent, false)
        return ArtistViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArtistViewHolder, position: Int) {
        val item = listItem[position]
        holder.artistName.text = item.artist.name
        holder.debut.text = item.artist.debut
        Log.d("artist", item.toString())
        holder.itemView.setOnClickListener{
            listener.OnItemClicked(item)
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class ArtistViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var artistName = itemView.findViewById<TextView>(R.id.artistName)
        var debut = itemView.findViewById<TextView>(R.id.debut)
    }

    interface ArtistListener {
        fun OnItemClicked(artist: ArtistWithSong)
    }
}