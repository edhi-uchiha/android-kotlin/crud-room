package com.enigmacamp.crud_room.room.artist

import androidx.room.*

@Dao
interface ArtistDao {

    @Insert
    fun create(artist: Artist)

    @Update
    fun update(artist: Artist)

    @Delete
    fun delete(artist: Artist)

    @Query("SELECT * FROM artist")
    fun getAll(): List<Artist>

    @Query("SELECT * FROM artist WHERE id = :id")
    fun getById(id: Int): List<Artist>
}