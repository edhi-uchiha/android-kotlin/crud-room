package com.enigmacamp.crud_room.room.artist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.enigmacamp.crud_room.R
import com.enigmacamp.crud_room.room.ArtistRoomDatabase
import kotlinx.android.synthetic.main.activity_artist_form.*

class artist_form : AppCompatActivity() {

    val EDIT_ARTIS_EXTRA = "edit_artist_extra"
    private lateinit var artist: Artist
    private var isUpdate = false
    private lateinit var database: ArtistRoomDatabase
    private lateinit var dao: ArtistDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_form)

        database = ArtistRoomDatabase.getDatabase(applicationContext)
        dao = database.getArtistDao()
        btn_delete.visibility = View.GONE

        if(intent.getParcelableExtra<Artist>(EDIT_ARTIS_EXTRA) != null){
            btn_delete.visibility = View.VISIBLE
            isUpdate = true
            artist = intent.getParcelableExtra(EDIT_ARTIS_EXTRA)
            et_artistName.setText(artist.name)
            et_debut.setText(artist.debut)

            et_artistName.setSelection(artist.name.length)

        }

        btn_submit.setOnClickListener{
            val name = et_artistName.text.toString()
            val debut = et_debut.text.toString()

            if (name.isEmpty() && debut.isEmpty()){
                Toast.makeText(applicationContext, "Artist cannot be empty", Toast.LENGTH_SHORT).show()
            } else {
                    if (isUpdate){
                        saveArtist(Artist(id = artist.id, name = name,debut = debut))
                    }
                    else{
                        saveArtist(Artist( name = name,debut = debut))
                    }
                }

                finish()
        }

        btn_delete.setOnClickListener {
            deleteArtist(artist)
            finish()
        }
    }

    private fun saveArtist(artist: Artist){
        if(dao.getById(artist.id).isEmpty()){
            dao.create(artist)
        }else{
            dao.update(artist)
        }

        Toast.makeText(applicationContext, "Artist saved", Toast.LENGTH_SHORT).show()
    }

    private fun deleteArtist(artist: Artist){
        dao.delete(artist)
        Toast.makeText(applicationContext, "Artist removed", Toast.LENGTH_SHORT).show()
    }
}