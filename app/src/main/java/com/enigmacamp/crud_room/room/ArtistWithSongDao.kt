package com.enigmacamp.crud_room.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction

@Dao
interface ArtistWithSongDao {

    @Query("SELECT * FROM Artist")
    @Transaction
    fun fetchArtistWithSongs(): List<ArtistWithSong>
}