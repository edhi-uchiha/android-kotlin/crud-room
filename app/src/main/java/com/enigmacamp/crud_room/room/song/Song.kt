package com.enigmacamp.crud_room.room.song

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.enigmacamp.crud_room.room.artist.Artist
import kotlinx.android.parcel.Parcelize

@Entity(
    foreignKeys = [
    ForeignKey(
        entity = Artist::class,
        childColumns = ["authorId"],
        parentColumns = ["id"]
    )]
)
@Parcelize
data class Song(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "authorId") var authorId: String
) : Parcelable {
}