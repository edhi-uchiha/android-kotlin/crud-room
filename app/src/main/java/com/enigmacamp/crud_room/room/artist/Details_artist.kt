package com.enigmacamp.crud_room.room.artist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import com.enigmacamp.crud_room.R
import com.enigmacamp.crud_room.room.ArtistWithSong
import com.enigmacamp.crud_room.room.song.Song
import com.enigmacamp.crud_room.song_form
import kotlinx.android.synthetic.main.list_artist.*

class details_artist : AppCompatActivity() {

    val ARTIS_EXTRA = "artist"
    private lateinit var item: ArtistWithSong
    private lateinit var songs: List<Song>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_artist)

       getData()
    }

    fun addSong(v: View){
        var intent: Intent = Intent(this, song_form::class.java)
        Log.d("auhtorIdDetails", item.artist.id.toString())
        intent.putExtra("authorId", item.artist.id.toString())
        startActivity(intent)
    }

    fun getData(){
        if(intent.getParcelableExtra<Artist>(ARTIS_EXTRA) != null) {
            item = intent.getParcelableExtra(ARTIS_EXTRA)
            artistName.text = item.artist.name
            debut.text = item.artist.debut
        }

        songs = item.songs

        Log.d("songs", songs.toString())

        val adapter = ArrayAdapter(this,
            R.layout.list_view_item, songs)

        val listView:ListView = findViewById(R.id.listSong)
        listView.setAdapter(adapter)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }
}