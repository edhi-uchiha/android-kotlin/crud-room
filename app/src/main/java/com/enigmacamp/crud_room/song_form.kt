package com.enigmacamp.crud_room

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.enigmacamp.crud_room.room.ArtistRoomDatabase
import com.enigmacamp.crud_room.room.artist.Artist
import com.enigmacamp.crud_room.room.artist.ArtistDao
import com.enigmacamp.crud_room.room.song.Song
import com.enigmacamp.crud_room.room.song.SongDao
import kotlinx.android.synthetic.main.activity_artist_form.*
import kotlinx.android.synthetic.main.activity_artist_form.btn_delete
import kotlinx.android.synthetic.main.activity_artist_form.btn_submit
import kotlinx.android.synthetic.main.activity_song_form.*

class song_form : AppCompatActivity() {

    val EDIT_SONG_EXTRA = "edit_song_extra"
    private lateinit var song: Song
    private var isUpdate = false
    private lateinit var database: ArtistRoomDatabase
    private lateinit var dao: SongDao
    private lateinit var authorId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_form)

        database = ArtistRoomDatabase.getDatabase(applicationContext)
        dao = database.getSongDao()
        btn_delete.visibility = View.GONE
        authorId = intent.getStringExtra("authorId")
        Log.d("authorId", authorId)

        if(intent.getParcelableExtra<Song>(EDIT_SONG_EXTRA) != null){
            btn_delete.visibility = View.VISIBLE
            isUpdate = true
            song = intent.getParcelableExtra(EDIT_SONG_EXTRA)
            et_title.setText(song.title)

            et_artistName.setSelection(song.title.length)

        }

        btn_submit.setOnClickListener{
            val title = et_title.text.toString()

            Log.d("title", title)

            if (title.isEmpty()){
                Toast.makeText(applicationContext, "Title cannot be empty", Toast.LENGTH_SHORT).show()
            } else {
                if (isUpdate){
                    saveSong(Song(id = song.id, title = title, authorId = authorId))
                }
                else{
                    saveSong(Song(title = title, authorId = authorId))
                }
            }

            finish()
        }

        btn_delete.setOnClickListener {
            deleteSong(song)
            finish()
        }
    }

    private fun saveSong(song: Song){
        if(dao.getById(song.id).isEmpty()){
            dao.create(song)
        }else{
            dao.update(song)
        }

        Toast.makeText(applicationContext, "Song saved", Toast.LENGTH_SHORT).show()
    }

    private fun deleteSong(song: Song){
        dao.delete(song)
        Toast.makeText(applicationContext, "Song removed", Toast.LENGTH_SHORT).show()
    }
}
