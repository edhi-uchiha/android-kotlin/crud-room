package com.enigmacamp.crud_room

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.enigmacamp.crud_room.room.ArtistRoomDatabase
import com.enigmacamp.crud_room.room.ArtistWithSong
import com.enigmacamp.crud_room.room.artist.Artist
import com.enigmacamp.crud_room.room.artist.ArtistAdaper
import com.enigmacamp.crud_room.room.artist.artist_form
import com.enigmacamp.crud_room.room.artist.details_artist
import com.enigmacamp.crud_room.room.song.Song
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getArtistData()

    }

    fun handleAddButton(v:View){
        var intent: Intent = Intent(this, artist_form::class.java)
        startActivity(intent)
    }

    fun getArtistData(){
        val database = ArtistRoomDatabase.getDatabase(applicationContext)
        val dao = database.getArtishWithSongDao()
        val listItem = arrayListOf<ArtistWithSong>()
        listItem.addAll(dao.fetchArtistWithSongs())
        setupRecycleView(listItem)
        if(listItem.isNotEmpty()){
            text_view_artist_empty.visibility = View.GONE
        } else {
            text_view_artist_empty.visibility = View.VISIBLE
        }

    }

    fun setupRecycleView(artists: ArrayList<ArtistWithSong>){
        artists_RV.apply {
            adapter = ArtistAdaper(artists, object : ArtistAdaper.ArtistListener{
                override fun OnItemClicked(artistWithSong: ArtistWithSong) {
                    val intent = Intent(this@MainActivity, details_artist::class.java)
                    intent.putExtra(details_artist().ARTIS_EXTRA, artistWithSong)
                    startActivity(intent)
                }

            })

            layoutManager = LinearLayoutManager(this@MainActivity)
        }

    }

    override fun onResume() {
        super.onResume()
        getArtistData()
    }

}